open Td;;

let in_bounds (row, col) =
  row >= 0
  && col >= 0
  && row < Const.height
  && col < Const.width
;;

let directions = [|
    (0, 1);
    (1, 1);
    (1, 0);
    (1, -1);
    (0, -1);
    (-1, -1);
    (-1, 0);
    (-1, 1);
  |]
;;

let empty tile =
  tile = `Empty
;;

let enemy state tile =
  not ((tile = `Empty) || (tile = state.my_id))
;;

let place_tile state tile (row, col) =
  state.grid.(row).(col) <- tile
;;

let remove_tile state (row, col) =
  state.grid.(row).(col) <- `Empty
;;

let calc_flip grid tile (row, col) =
  let result = ref [] in
  let sub_result = ref [] in
  let rec check_flip (row, col) first_step (off_row, off_col) =
    let tr, tc = (row + off_row, col + off_col) in
      if not (in_bounds (tr, tc)) then
        sub_result := []
      else
        if grid.(tr).(tc) = tile then
         (
(*          Debug.debug (Printf.sprintf "flip of %d possible\n" (List.length !sub_result)); *)
          result := !result @ !sub_result;
          sub_result := []
         )
        else if grid.(tr).(tc) = `Empty then sub_result := []
(* Debug.debug "flip failed\n"*)
        else
         (
(*          Debug.debug "flip prepared\n"; *)
          sub_result := (tr, tc) :: !sub_result;
          check_flip (tr, tc) false (off_row, off_col)
         )
  in
    Array.iter (check_flip (row, col) true) directions;
    !result
;;

let does_flip grid tile (row, col) =
  let flipped = calc_flip grid tile (row, col) in
  let num = List.length flipped in
(*    Debug.debug (Printf.sprintf "moving at %d, %d flipped %d\n" row col num);
*)
    num > 0
;;

let all_empty grid =
  let result = ref [] in
    Array.iteri (fun ir row -> Array.iteri (fun ic t ->
      if (t = `Empty) then
        result := (ir, ic) :: !result
    ) row) grid;
  !result
;;

let valid_moves state player =
  let m = all_empty state.grid in
    List.filter (does_flip state.grid state.my_id) m
(*
  let m = all_moves state player in
  let attack = List.filter (fun (m, atck) -> atck) m in
    if attack = [] then m else attack
*)
;;

