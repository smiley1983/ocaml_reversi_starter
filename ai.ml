open Td;;

(* pick a random item from a list *)
let random_choice l =
  let num = List.length l in
  let choice = Random.int num in
    List.nth l choice
;;

(* get the list of valid moves and return a random one *)
let engine state = 
  let valid = Game.valid_moves state state.my_id in
  let chosen = random_choice valid in
    Io.issue_order chosen
;;

