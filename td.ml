type tile = [ `Dark | `Light | `Empty ]

(*type dir = [ `N | `E | `S | `W | `NE | `SE | `NW | `SW ];;*)

type state =
 {
  grid : tile array array;
  mutable row_count : int;
  mutable my_id : tile;
 }
;;

